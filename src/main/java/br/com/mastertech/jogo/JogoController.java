package br.com.mastertech.jogo;

import br.com.mastertech.jogo.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JogoController {

    @GetMapping("/{titulo}")
    public Jogo create(@PathVariable String titulo, @AuthenticationPrincipal Usuario usuario) {
        Jogo jogo = new Jogo();
        jogo.setTitulo(titulo);
        jogo.setJogador(usuario.getName());
        return jogo;
    }
}
